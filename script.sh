db() {
 docker run -P --volumes-from app_data --name koresol_db -e POSTGRES_USER=app_user -e POSTGRES_PASSWORD=admin123 -t -d postgres:latest
}

app() {
# docker stop koresolapp
 # docker rm koresolapp
  docker run -p 80:80 --link koresol_db:postgres --name koresolapp koresol/app
}

action=$1
${action}
