git clone --depth 1 http://bitbucket.org/koresol/dockerapp app
cd app

source "/usr/local/share/chruby/chruby.sh"

#source "/usr/local/share/chruby/auto.sh"
chruby ruby

gem install bundler

#gem install tzinfo-data
bundle install  --without=development,test
bundle exec rake db:migrate

if [[ $? != 0 ]]; then
  echo
  echo "== Failed to migrate. Running setup first."
  echo
  bundle exec rake db:setup && \
  bundle exec rake db:migrate
fi

export SECRET_KEY_BASE=$(rake secret)

sudo rm /etc/nginx/conf.d/*
sudo ln -s /home/app/nginx.conf /etc/nginx/conf.d/app.conf

sudo service nginx start

bundle exec rake assets:precompile

bundle exec puma -e production -b unix:///home/app/puma.sock
